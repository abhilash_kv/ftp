package com.pibro.ftp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by Abhi on 6/11/2016.
 */
public class Main extends AppCompatActivity{
    private ProgressDialog progressDialog;
    private Button CreateTxt_bt;
    private Button Ftp_bt;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        context = this;
        Init();
        Listeners();

    }

    private void Listeners() {
        CreateTxt_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Create_txt(context, "test.txt", "test content");
            }
        });
        Ftp_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SendFtp("host", "username", "password", "pathname", "test.txt");
                progressDialog = ProgressDialog.show(context, "", "Send File...", true);
                new SendFtp().execute(context,"smartsurvey.net23.net", "a3389349", "rameest5", "public_html/Ftp", "Notes/test.txt");
            }
        });
    }
    private void Init() {
        CreateTxt_bt = (Button)findViewById(R.id.createtxt);
        Ftp_bt = (Button)findViewById(R.id.ftp);
    }
    private void SendFtp(String host, String username, String password, String pathname, String filename) {
        FTPClient ftpClient = new FTPClient();
        try {
            File file = new File(Environment.getExternalStorageDirectory(),filename);
            ftpClient.connect(InetAddress.getByName(host));
            ftpClient.login(username, password);
            ftpClient.changeWorkingDirectory(pathname);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            BufferedInputStream buffIn=null;
            buffIn=new BufferedInputStream(new FileInputStream(file));
            ftpClient.enterLocalPassiveMode();
            ftpClient.storeFile("test.txt", buffIn);
            buffIn.close();
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void Create_txt(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(),"Notes");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void FtpResponse(Boolean result) {
        progressDialog.dismiss();
        if(result)
            Toast.makeText(context,"File Uploaded",Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(context,"File Not Uploaded Please try anagin...",Toast.LENGTH_SHORT).show();
    }
}
