package com.pibro.ftp;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by Abhi on 6/11/2016.
 */

public class SendFtp extends AsyncTask<Object, Void, Boolean> {
    Object object;
    String host;
    String username;
    String password;
    String pathname;
    String filename;

    @Override
    protected Boolean doInBackground(Object... params)
    {
        object = params[0];
        host = (String)params[1];
        username = (String)params[2];
        password = (String)params[3];
        pathname = (String)params[4];
        filename = (String)params[5];
        FTPClient ftpClient = new FTPClient();
        try {
            File file = new File(Environment.getExternalStorageDirectory(),filename);
            ftpClient.connect(InetAddress.getByName(host));
            ftpClient.login(username, password);
            ftpClient.changeWorkingDirectory(pathname);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            BufferedInputStream buffIn;
            buffIn=new BufferedInputStream(new FileInputStream(file));
            ftpClient.enterLocalPassiveMode();
            ftpClient.storeFile("test.txt", buffIn);
            buffIn.close();
            ftpClient.logout();
            ftpClient.disconnect();
            return true;
        } catch (Exception e) {
            Log.e("Ftp", "" + e.toString());
            return false;
        }
    }
    @Override
    protected void onPostExecute(Boolean result) {
        try
        {
            ((Main)object).FtpResponse(result);
        }catch(Exception e)
        {
        }

    }
}
